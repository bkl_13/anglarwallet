<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    protected $table = 'users';
    protected $primaryKey = "user_id";
    
    protected $fillable = [
        'user_uname', 'user_email', 'user_pword', 'user_firstname', 'user_lastname', 'user_currency', 'is_actived'
    ];

    protected $hidden = [
        'user_pword', 'remember_token', 'activation_key',
    ];

    //Mot User co the co nhieu Campaigns
    public function campaign() {
        return $this->hasMany('App\Campaign');
    }

    public function category(){
        return $this->hasMany('App/Category');
    }

    public function wallet(){
        return $this->hasMany('App/Wallet');
    }

    public function transaction(){
        return $this->hasMany('App/Transaction');
    }
}
