<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class People extends Model
{
    protected $table = 'people';
    protected $primaryKey = 'people_id';

    protected $fillable = [
        'user_id', 'people_name', 'people_phone'
    ];

    //The hien moi quan he many - many giua Peoples va Transactions
    //Tham so 1 - 'transaction_people' : ten bang pivot lien ket giua Peoples va Transactions
    //Tham so 2 -  'people_id' : ten khoa xac dinh cua doi tuong hien tai, dung cho viec tim kiem trong bang pivot.
    //Tham so 3 -  'trans_id' : ten khoa xac dinh cua doi tuong Transaction.
    public function transactions() {
        return $this->belongsToMany('App\Transaction', 'transaction_people', 'people_id', 'trans_id');
    }
}
