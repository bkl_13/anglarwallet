<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transactions';
	protected $primaryKey = 'trans_id';

	protected $fillable = [
		'user_id',
		'wallet_id',
		'cat_id',
		'trans_amount',
		'trans_note',
		'trans_time'
	];

	protected $hidden = [
		'trans_search'
	];

   	


   	public function category()
   	{
   		return $this->belongsTo('App\Category', 'cat_id');
   	}

   	public function wallet()
   	{
   		return $this->belongsTo('App\Wallet', 'wallet_id');
   	}

   	public function campaigns(){
   		return $this->belongsToMany('App\Campaign', 'transaction_campaign', 'trans_id', 'cam_id');
   	}
   	

	public function people() {
		return $this->belongsToMany('App\People', 'transaction_people', 'trans_id', 'people_id');
	}
}
