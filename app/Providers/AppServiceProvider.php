<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //Cac ham so sanh voi gia tri hoac field khac, dung cho Validator
        Validator::extend('greater_than_field', function($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            return $value > $min_value;
        });

        Validator::extend('greater_than', function($attribute, $value, $parameters, $validator) {
            $min_value = $parameters[0];
            return $value > $min_value;
        });

        Validator::extend('less_than_field', function($attribute, $value, $parameters, $validator) {
            $min_field = $parameters[0];
            $data = $validator->getData();
            $min_value = $data[$min_field];
            return $value > $min_value;
        });

        Validator::extend('less_than', function($attribute, $value, $parameters, $validator) {
            $min_value = $parameters[0];
            return $value > $min_value;
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
      if ($this->app->environment() == 'local') {
          $this->app->register('Laracasts\Generators\GeneratorsServiceProvider');
      }
    }
}
