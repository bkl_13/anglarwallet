<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Wallet;
use App\Transaction;
use App\Category;
use App\Http\Requests;
use JWTAuth;
use Illuminate\Support\Facades\Validator;
class WalletController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }


    public function index()
    {
        $flag =true ;

        $data = array();

        $message = '';

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);

        $rows = array();
        if($flag){
            $wallet = Wallet::where('user_id', $user->user_id)->get();
            foreach ($wallet as $item) {
                $rows[] = array(
                    'wallet_id' => $item->wallet_id,
                    'wallet_name' => $item->wallet_name,
                    'wallet_description' => $item->wallet_description
                );
            }
            $data[] = ['wallet' => $rows];
        }else{
            $errors[] = $user;
            $message = 'Loi';
            $code = 401;
        }
        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }

    public function store(Request $request)
    {
        $flag =true ;

        $data = array();

        $message = '';

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);

        if($flag){
            $validator = Validator::make($request->all(),[
                'wallet_name' => 'required|min:5|max:255|string',
                'wallet_description' => 'max:1000|string',
                'start_point' => 'integer|greater_than:0'
            ]);

            if($validator->fails()){
                $flag = false;
                $message = 'Them khong thanh cong';
                $errors[] = array( 'validator' => $validator->errors() ) ;
                $code = 400;
            }else{
                $wallet = new Wallet;
                $wallet->user_id = $user->user_id;
                $wallet->wallet_name = $request->wallet_name;
                $wallet->wallet_description = $request->wallet_description;
                $wallet->save();
                $wallet_id = $wallet->wallet_id;
                if ($wallet_id) {
                    $cat = Category::where('user_id', $user->user_id)->where('meta_data', 'etc')->first();
                    if ($cat) {
                        $trans = new Transaction;
                        $trans->user_id = $user->user_id;
                        $trans->wallet_id = $wallet_id;
                        $trans->cat_id = $cat->cat_id;
                        $trans->trans_amount = $request->start_point;
                        $trans->trans_note = 'Số tiền khởi tạo của ' . $request->wallet_name;
                        $trans->trans_time = date("Y-m-d H:i:s");
                        $trans->trans_search = $this->vn_str_filter($trans->trans_note);
                        $trans->save();
                    }
                }
                $message = 'inserted successfully!';
            }
        }else{
            $errors[] = $user;
            $message = 'Loi';
            $code = 401;
        }

        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }


    public function show($id)
    {

        $flag =true ;

        $message = '';

        $data = array();

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);

        if($flag){
            if(!$Wallet = Wallet::where([ 'user_id'=>  $user->user_id, 'wallet_id' => $id])->first()){
                $flag =false ;
                $message = 'Khong tim thay';
                $errors[] = 'Khong ton tai wallet';
                $code = 404;
            }else{
                $data[] = ['wallet' => [
                    'wallet_id' => $Wallet->wallet_id,
                    'wallet_name' => $Wallet->wallet_name,
                    'wallet_description' => $Wallet->wallet_description
                ]];
            }
                
        }else{
            $errors[] =$user ; // khi user bi loi tra ve exception
            $message = 'Loi';
            $code = 401;
        }
        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }
    public function update(Request $request, $id)
    {
        $flag =true ;

        $message = '';

        $data = array();

        $errors = array();

        $code = 200;

        $user = $this->getUser($flag);

        if($flag){
            if(!$wallet = Wallet::Where(['user_id' => $user->user_id, 'wallet_id' => $id])->first()){
                $flag = false;
                $message = 'Loi';
                $errors[] = 'Khong ton tai wallet';
                $code = 404;
            }else{
                $validator = Validator::make($request->all(),[
                    'wallet_name' => 'required|min:5|max:255|string',
                    'wallet_description' => 'required|max:1000|string'
                ]);

                if($validator->fails()){
                    $flag = false;
                    $message = 'Khong cap nhat duoc';
                    $errors[] =['validator' => $validator->errors()];
                    $code = 400;
                }else{
                    $wallet = Wallet::find($id);
                    $wallet->wallet_name = $request->wallet_name;
                    $wallet->wallet_description = $request->wallet_description;
                    $wallet->save();
                    $message = 'Cap nhat thanh cong!';
                } 
            }

        }else{
            $errors[] =$user ; // khi user bi loi tra ve exception
            $message = 'Loi';
            $code = 401;        
        }
        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }

    public function destroy($id)
    {
        $flag =true ;

        $message = '';

        $data = array();

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);

        if($flag){

            if(!$wallet = Wallet::Where(['user_id' => $user->user_id, 'wallet_id' => $id])->first()){
                $flag = false;
                $message = 'Loi';
                $errors[] = 'Khong ton tai wallet';
                $code = 404;
            }else{
                $trans = Wallet::find($id)->transactions;
                foreach ($trans as $tran) {
                    $tran->delete();
                }
                $wallet = Wallet::find($id);
                $wallet->delete();
                $message = 'Xoa thanh cong';
            }
        }else{
            $errors[] =$user ; // khi user bi loi tra ve exception
            $message = 'Loi';
            $code = 401;  
        }

        return Response()->json($this->returnResponse($flag, $message, $data, $errors),$code);
    }


}
