<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Http\Response;
use JWTAuth;

use App\People;

class PeopleController extends Controller
{
    public function __construct(){
        $this->middleware('jwt.auth', [ 'except' => [''] ]);
    }

    function listTransactions($people_id) {
        $status_code = '400';
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();

        $result = $this->getUser($flag);
        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $user_id = $user->user_id;

            //Tim People co ma people_id phu hop ma tai khoan dang dang nhap co quyen truy xuat
            $check =  !empty(People::select('user_id')->where('people_id', $people_id)->where('user_id', $user_id)->first());
            if ($check) {
                $tr = People::find($people_id)->transactions;
                if (!empty($tr)) {
                    $message = 'Da tim thay';
                    $data['transactions'] = $tr;
                    $status_code = '200';
                } else {
                    $flag = false;
                    $message = 'Khong tim thay';
                    $errors[] = 'Khong tim thay giao dich nao';
                    $status_code = '404';
                }
            } else {
                $flag = false;
                $message = 'Khong co quyen truy cap';
                $errors[] = 'Khong co quyen xem thong tin nguoi nay';
                $status_code = '403';
            }
        } else {
            $message = 'Loi';
            $errors[] = $result;
            $status_code = '406';
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }
}
