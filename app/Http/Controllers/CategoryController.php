<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use App\Category;
use Illuminate\Http\Response;
use Illuminate\Database\Eloquent\ModelNotFoundException;


class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    public function index()
    {
        $flag = true;

        $data = array();

        $message = '';

        $errors = array();
        $code =200;
        $user = $this->getUser($flag);

        if($flag){
            $cat = Category::where('user_id', $user->user_id)->get();
            $rows = array();
            foreach ($cat as $item) {
                $rows[] = [
                    'cat_id' => $item->cat_id,
                    'cat_name' => $item->cat_name,
                    'cat_type' => $item->cat_type,
                    'meta_data' => $item->meta_data];
            }

            $data[] = ['category' => $rows];

        }else{
            $errors[] = $user;
            $message='Loi';
            $code = 404;
        }
        

        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }

    public function store(Request $request)
    {

        $flag =true ;

        $data = array();

        $message = '';

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);

        if($flag){
            $validator = Validator::make($request->all(),[
                'cat_name' => 'required|min:5|max:255|string',
                'cat_type' => 'required|integer|in:1,2'
                ]);

            if($validator->fails()){
                $flag = false ;
                $message = 'them khong thanh cong';
                $errors[] =['validator' => $validator->errors()];
                $code = 404;
            }else{
                $cat = new Category;
                $cat->user_id = $user->user_id;
                $cat->cat_name = $request->cat_name;
                $cat->cat_type = $request->cat_type;
                $cat->save();
                $message = 'them thanh cong';
            }


        }else{
            $message = 'Loi';
            $code = 404;
            $errors[] = $user;
        }

        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);

    }


    public function show($id)
    {

        $flag =true ;

        $message = '';

        $data = array();

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);
        if($flag){
            $where = [ 'user_id'=>  $user->user_id, 'cat_id' => $id];
            if(!$category = Category::where($where)->first()){ // lay phan tu dau tien
                $errors[] ='khong ton tai';
                $message = 'Loi';
                $code = 404;
                $flag = false;
            }else{
                $data[] = [ 'category' => [
                    'cat_id' => $category->cat_id,
                    'cat_name' => $category->cat_name,
                    'cat_type' => $category->cat_type,
                    'meta_data' => $category->meta_data
                    ]];
            }
        }else{
            $message = 'Loi';
            $code = 404;
            $errors[] = $user;

        }
        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }
    public function update(Request $request, $id)
    {

        $flag =true ;

        $message = '';

        $data = array();

        $errors = array();
        $code =200;
        $user = $this->getUser($flag);

        if($flag){
            if(!$cat = Category::where(['user_id'=> $user->user_id, 'cat_id'=>$id])->first()){
                $errors[] = 'khong ton tai';
                $message = 'Loi';
                $flag= false;
                $code = 404;
            }else{
                $validator = Validator::make($request->all(),[
                    'cat_name' => 'required|min:5|max:255|string',
                    'cat_type' => 'required|integer|in:1,2',
                    'meta_data' => 'required|min:5|max:255|string'
                    ]);

                if($validator->fails()){
                    $flag = false ;
                    $message = 'cap nhat khong thanh cong';
                    $errors[] =['validator' => $validator->errors()];
                    $code = 404;
                }else{
                    $cat = Category::find($id);
                    $cat->cat_name = $request->cat_name;
                    $cat->cat_type = $request->cat_type;
                    $cat->meta_data = $request->meta_data;
                    $cat->save();
                    $message = 'cap nhat thanh cong';
                }
            }
        }else{
            $message = 'Loi';
            $code = 404;
            $errors[] = $user;

        }
        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }

    public function destroy($id)
    {

        $flag =true ;

        $message = '';

        $data = array();

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);

        if($flag){
            if(!$cat = Category::where(['user_id'=> $user->user_id, 'cat_id'=>$id])){
                $flag =false ;
                $message = 'Khong cap nhat duoc';
                $errors[] = 'Khong ton tai';
                $code = 404;
            }else{
                $trans = Category::find($id)->transactions;
                foreach ($trans as $tran) {
                    $tran->delete();
                }
                $cat = Category::delete($id);
                $message = 'Da xoa xong';
            }
        }else{
            $message = 'Loi';
            $code = 404;
            $errors[] = $user;

        }
        return Response()->json($this->returnResponse($flag, $message, $data, $errors),$code);
    }
}
