<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;
use JWTAuth;
use App\Http\Requests;
use App\Transaction;
use App\Wallet;
use App\Category;
class TransactionController extends Controller
{
    //
    public function __construct(){
        $this->middleware('jwt.auth');
    }

	public function index()
	{
		$flag =true ;

		$data = array();

		$message = '';

		$errors = array();

		$code =200;

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if($flag){
    		$rows = array();
    		$trans = Transaction::with(['category' => function($query){
                $query->select('cat_id', 'cat_name', 'cat_type');
            }])->where('user_id', $user->user_id)->orderBy('trans_time', 'desc')->get();
    		foreach ($trans as $item) {
    			$rows[] = array(
    				'trans_id' => $item->trans_id,
    				'trans_amount' => $item->trans_amount,
    				'trans_note' => $item->trans_note,
    				'trans_search' => $item->trans_search,
    				'trans_relation' => $item ->trans_relation,
    				'trans_time' => date('d-m-Y', strtotime($item ->trans_time)),
                    'cat' => array(
                        'cat_id' => $item['category']['cat_id'],
                        'cat_name' => $item['category']['cat_name'],
                        'cat_type' => $item['category']['cat_type']
                    )
                );
    		}
    		$data[] = ['trans' => $rows];
        }else{
            $message = "user not found!";
            $errors[] = $result;
            $code = 406;
        }
		return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
	}

    public function test() {
        echo "ABC";
    }

	public function store(Request $request)
	{
		$flag =true ;

		$data = array();

		$message = '';

		$errors = array();

		$code =200;
        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;
        
        if($flag){
    		$validator = Validator::make($request->all(),[
    			'trans_amount' => 'required|integer|greater_than:0',
    			'trans_note' => 'max:120|string',
    			'trans_relation' => 'integer|greater_than:0',
    			'trans_time' => 'required|date_format:Y-m-d H:i:s',
                'cat_id' => 'required|exists:categories,cat_id|integer',
                'wallet_id' => 'required|exists:wallets,wallet_id|integer'
    			]);

    		if($validator->fails()){
    			$flag = false;
    			$message = 'Them khong thanh cong';
    			$errors[] = array( 'validator' => $validator->errors() ) ;
    			$code = 404;
    		}else{
    			$trans = new Transaction;
    			$trans->user_id = $user->user_id;
    			$trans->wallet_id = $request->wallet_id;
    			$trans->cat_id = $request->cat_id;
    			$trans->trans_amount = $request->trans_amount;
    			$trans->trans_note = $request->trans_note;
    			$trans->trans_relation = $request->trans_relation;
    			$trans->trans_time = $request->trans_time;
    			$trans->trans_search = $this->vn_str_filter($trans->trans_note);
    			$trans->save();
    			$message = 'inserted successfully!';
    		}
        }else{
            $message = "user not found!";
            $errors[] = $result;
            $code = 406;
        }

		return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
	}


	public function show($id)
	{

		$flag =true ;

		$message = '';

		$data = array();

		$errors = array();

		$code =200;

		$result = $this->getUser($flag);

        $user = $flag ? $result : NULL;
        
        if($flag){
    		if(!$trans = Transaction::where(['user_id'=> $user->user_id, 'trans_id' => $id])->first()){
                $errors = 'Khong ton tai giao dich';
                $message = 'Loi';
                $code = 404;
                $flag = false;
            }else{
    			
            	$data[] = ['trans' => [
            	'trans_id' => $trans->trans_id,
                'wallet_id' => $trans->wallet_id,
                'cat_id' => $trans->cat_id,
            	'trans_amount' => $trans->trans_amount,
            	'trans_note' => $trans->trans_note,
            	'trans_relation' => $trans->trans_relation,
            	'trans_time' => date('d-m-Y', strtotime($trans->trans_time))
            	]];
            }
        }else{
            $message = "user not found!";
            $errors[] = $result;
            $code = 406;
        }

        return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }
    public function update(Request $request, $id)
    {
    	$flag =true ;

    	$message = '';

    	$data = array();

    	$errors = array();

        $code =200;

    	$result = $this->getUser($flag);

        $user = $flag ? $result : NULL;
        
        if($flag){
            if(!$trans = Transaction::where(['user_id'=> $user->user_id, 'trans_id' => $id])->first()){
                $errors[] = 'khong ton tai giao dich';
                $message = 'Loi';
                $code = 404;
                $flag = false;

            }else{
                $wallet = Wallet::where(['user_id'=> $user->user_id, 'wallet_id'=>$request->wallet_id])->first();
                $cat = Category::where(['user_id'=> $user->user_id, 'cat_id'=>$request->cat_id])->first();
                if(!$wallet || !$cat){
                    $errors = 'Khong ton tai wallet, cat';
                    $message = 'Loi';
                    $flag = false;
                    $code = 404;
                }else{
            		$validator = Validator::make($request->all(),[
            			'trans_amount' => 'required|integer|greater_than:0',
                        'trans_note' => 'max:120|string',
                        'trans_relation' => 'integer|greater_than:0',
                        'trans_time' => 'required|date_format:Y-m-d H:i:s',
                        'cat_id' => 'required|exists:categories,cat_id|integer',
                        'wallet_id' => 'required|exists:wallets,wallet_id|integer'
            			]);
            		if($validator->fails()){
            			$flag = false;
            			$message = 'Khong cap nhat duoc';
            			$errors[] =['validator' => $validator->errors()];
            			$code = 404;
            		}else{
            			$trans = Transaction::find($id);
            			$trans->trans_amount = $request->trans_amount;
            			$trans->trans_note = $request->trans_note;
            			$trans->trans_relation = $request->trans_relation;
                        $trans->cat_id = $request->cat_id;
                        $trans->trans_time = $request->trans_time;
        				$trans->wallet_id = $request->wallet_id;
            			$trans->trans_search = $this->vn_str_filter($trans->trans_note);
            			$trans->save();
            			$message = 'Cap nhat thanh cong!';
            		}
                }           
        	}
        }else{
            $message = "user not found!";
            $errors[] = $result;
            $code = 406;
        }

    	return Response()->json($this->returnResponse($flag, $message, $data, $errors), $code);
    }

    public function destroy($id)
    {
    	$flag =true ;

    	$message = '';

    	$data = array();

    	$errors = array();

    	$code =200;

    	$result = $this->getUser($flag);

        $user = $flag ? $result : NULL;
        
        if($flag){
        	if(!$trans = Transaction::where(['user_id'=> $user->user_id, 'trans_id' => $id])->first()){
        		$flag =false ;
                $message = 'Khong xoa duoc';
                $errors[] = 'khong ton tai';
                $code = 404;
        	}else{
        		$trans->delete();
                $message = 'Xoa thanh cong';
        	}
        }else{
            $message = "user not found!";
            $errors[] = $result;
            $code = 406;
        }
    	return Response()->json($this->returnResponse($flag, $message, $data, $errors),$code);
    }

    public function search(Request $request){
        $flag =true ;

        $message = '';

        $data = array();

        $errors = array();

        $code =200;

        $user = $this->getUser($flag);

        if($flag) {
            $validator = Validator::make($request->all(), [
                'cat_id' => 'integer|greater_than:0',
                'wallet_id' => 'integer|greater_than:0',
                'days' => 'integer|greater_than:0',
                'cat_type' => 'in:1,2',
                'people_id' => 'integer|greater_than:0',
                'people_name' => 'string|min:5|max:40',
                'people_phone' => 'integer',
                'search' => 'string',
                'start_date' => 'date',
                'end_date' => 'date',
                'trans_amount' => 'integer|greater_than:0',
                'trans_type_amount' => 'string|in:=,>=,<=,<,>',
                'limit' => 'integer|greater_than:0'
            ]);

            if ($validator->fails()) {
                $errors[] = $validator->errors();
                $message = 'loi roi :v';
                $code = 400;
                $flag = false;
            } else {
                $cat_id = $request->cat_id ? $request->cat_id : '';
                $wallet_id = $request->wallet_id ? $request->wallet_id : '';
                $days = $request->days ? $request->days : '';
                $cat_type = $request->cat_type ? $request->cat_type : '';
                $people_id = $request->people_id ? $request->people_id : '';
                $people_name = $request->people_name ? $request->people_name : '';
                $people_phone = $request->people_phone ? $request->people_phone : '';
                $search = $request->search ? $request->search : '';
                $start_date = $request->start_date ? $request->start_date : '';
                $end_date = $request->end_date ? $request->end_date : '';
                $trans_amount = $request->trans_amount ? $request->trans_amount : '';
                $trans_type_amount = $request->trans_type_amount ? $request->trans_type_amount : '=';
                $limit = $request->limit ? $request->limit : 5;

                $q = Transaction::with(['category', 'wallet', 'people']);

                $q->where('cat_id', 'like', "%$cat_id%");
                $q->where('wallet_id', 'like', "%$wallet_id%");
                if ($cat_type != '')
                    $q->whereHas('category', function ($query) use ($cat_type) {
                        $query->where('cat_type', $cat_type);
                    });
                if ($people_phone != '')
                    $q->whereHas('people', function ($query) use ($people_phone) {
                        $query->where('people_phone', $people_phone);
                    });
                if ($people_name != '')
                    $q->whereHas('people', function ($query) use ($people_name) {
                        $query->where('people_name', $people_name);
                    });
                $q->Where(function ($query) use ($search) {
                    $query->where('trans_search', 'like', "%$search%");
                    $query->orwhere('trans_note', 'like', "%$search%");
                });

                if ($trans_amount != '') {
                    $q->where('trans_amount', "$trans_type_amount", $trans_amount);
                }
                if ($start_date != '' and $end_date != '') {
                    $q->whereRaw("trans_time between ('$start_date') and ('$end_date')");
                }

                $q->paginate($limit);

                return Response()->json($q->get());

            }
        }else{
            $errors[] = $user;
            $message = 'tai khoan ko ton tai';
            $code = 400;
        }
        return Response()->json($this->returnResponse($flag, $message, $data, $errors),$code);
    }

    function getSum(Request $request){
        $flag = true;

        $user = $this->getUser($flag);

        $ds = array();

        $tong = 0;
        if($flag){
            $start_date = $request->start_date ? $request->start_date : '';
            $end_date = $request->end_date ? $request->end_date : '';
            $meta_data =  $request->meta_data ? $request->meta_data : '';
            $validator = Validator::make($request->all(), [
                'start_date' => 'date',
                'end_date' => 'date',
                'meta_data' => 'string'
            ]);
            if($validator->fails()){

            }
            $trans = Transaction::with(['category'])->where('user_id', $user->user_id)->get();

            foreach ($trans as $tran){
                if($tran->category->cat_type==2){
                    $tong -= $tran->trans_amount;
                }else{
                    $tong += $tran->trans_amount;
                }
            }
        }

        return Response()->json([
            'flag' =>  true,
            'sum'=> $tong
        ]);
    }
}