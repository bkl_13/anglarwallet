<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class TestJWTController extends Controller
{
  public function __construct(){
    $this->middleware('jwt.auth');
  }

  public function index() {
    echo "ABC";
  }
}
