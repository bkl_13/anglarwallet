<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;

use App\Category;

use Validator;

use JWTAuth;

use Tymon\JWTAuth\Exceptions\JWTException;

class UserController extends Controller
{
    public function __construct()
    {
        //Tat ca cac xu ly deu phai duoc xac thuc, ngoai tru cac xu ly cho viec dang nhap va dang ky
        $this->middleware('jwt.auth', ['except' => ['signup', 'login']]);
    }

    public function signup(Request $request) {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        $user_data = $request->only('user_uname', 'user_pword', 'user_email', 'user_firstname', 'user_lastname');

        $messages = [
            'user_uname.required' => 'Chưa điền tên đăng nhập.',
            'user_uname.unique' => 'Tên đăng nhập này đã được sử dụng.',
            'user_uname.between' => 'Tên đăng nhập phải có :min - :max ký tự.',
            'user_pword.required' => 'Chưa điền mật khẩu.',
            'user_pword.between' => 'Mật khẩu phải có :min - :max ký tự.',
            'user_email.required' => 'Chưa điền địa chỉ email.',
            'user_email.email' => 'Không đúng định dạng email.',
            'user_email.unique' => 'Địa chỉ email này đã có người sử dụng.',
            'user_email.max' => 'Email vượt quá giới hạn ký tự tối đa (:max ký tự).',
            'user_firstname.required' => 'Chưa điền tên.',
            'user_firstname.max' => 'Tên có tối đa :max ký tự',
            'user_lastname.required' => 'Chưa điền họ & tên đệm.',
            'user_lastname.max' => 'Họ và tên đệm có tối đa :max ký tự',
        ];

        $rules = [
            'user_uname' => 'required|unique:users|between:4,60',
            'user_pword' => 'required|between:6,64',
            'user_email' => 'required|email|unique:users|max:100',
            'user_firstname' => 'required|max:100',
            'user_lastname' => 'required|max:100'
        ];

        $validator = Validator::make($user_data, $rules, $messages);

        if ($validator->fails()) {
            $flag = false;
            $message = 'Thong tin khong hop le';
            $errors[] = $validator->errors();
            $status_code = '406';
        } else {
            $newUser = User::create([
                'user_uname' => $user_data['user_uname'],
                'user_pword' => md5($user_data['user_pword']),
                'user_email' => $user_data['user_email'],
                'user_firstname' => $user_data['user_firstname'],
                'user_lastname' => $user_data['user_lastname'],
                'is_actived' => 1,
                'user_currency' => 'VNĐ'
            ]);

            $user_id = $newUser->user_id;

            $defaultCategory = Category::where('user_id', null)->get();

            foreach($defaultCategory as $cat) {
                Category::create([
                    'user_id' => $user_id,
                    'cat_name' => $cat['cat_name'],
                    'cat_type' => $cat['cat_type'],
                    'meta_data' => $cat['meta_data']
                ]);
            }

            $message = 'Da tao tai khoan thanh cong';
            $status_code = 200;
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }

    public function login(Request $request)
    {
        $credentials = $request->only('user_uname', 'user_pword');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Sai thông tin tài khoản hoặc mật khẩu'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function info() {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $message = 'Thong tin tai khoan';
            $data['user'] = $user;
            $status_code = '200';
        } else {
            $message = "Loi";
            $errors[] = $result;
            $status_code = '406';
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }

    //Xu ly cho route doi mat khau
    public function changepword(Request $request) {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        //Ket qua nhan duoc tu viec lay thong tin tai khoan da dang nhap tu token
        $result = $this->getUser($flag);

        //Lay thong tin khong thanh cong thi $user = NULL
        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $user_data = $request->only('user_uname', 'user_pword', 'newpword', 'newpword_confirmation');

            $validator = Validator::make($user_data, [
                'user_uname' => 'required|exists:users',
                'user_pword' => 'required',
                'newpword' => 'required|max:100|confirmed',
                'newpword_confirmation' => 'required|max:100'
            ]);

            if ($validator->fails()) {
                $flag = false;
                $message = 'Thong tin khong hop le';
                $errors[] = $validator->errors();
                $status_code = '406';
            } elseif ($user->user_uname != $user_data['user_uname']) {
                $flag = false;
                $message = 'Sai thong tin';
                $errors[] = 'Khong dung ten dang nhap';
                $status_code = '406';
            } elseif ($user->user_pword != md5($user_data['user_pword'])) {
                $flag = false;
                $message = 'Sai thong tin';
                $errors[] = 'Khong dung mat khau';
                $status_code = '406';
            } else {
                User::where('user_uname', $user->user_uname)
                    ->update(['user_pword' => md5($user_data['newpword'])]);
                $message = 'Da doi mat khau thanh cong';
                $status_code = '200';
            }
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }

    public function update(Request $request) {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $user_id = $user->user_id;
            $user_data = $request->only('user_firstname', 'user_lastname');
            $validator = Validator::make($user_data, [
                'user_firstname' => 'required_without_all:user_lastname|max:100',
                'user_lastname' => 'required_without_all:user_firstname|max:100'
            ]);

            $user_need_update = User::find($user_id);

            if ($validator->fails()) {
                $flag = false;
                $message = 'Thong tin khong hop le';
                $errors[] = $validator->errors();
                $status_code = '406';
            } elseif (empty($user_need_update)) {
                $flag = false;
                $message = 'Khong ton tai';
                $errors[] = 'Khong ton tai tai khoan nay';
                $status_code = '404';
            } else {
                $user_need_update->user_firstname = $user_data['user_firstname'] ? $user_data['user_firstname'] : $user->user_firstname;
                $user_need_update->user_lastname = $user_data['user_lastname'] ? $user_data['user_lastname'] : $user->user_lastname;
                $user_need_update->save();
                $message = 'Da cap nhat tai khoan';
                $data['user'] = $user_need_update;
                $status_code = 200;
            }
        } else {
            $message = "Loi";
            $errors[] = $result;
            $status_code = '406';
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }
}
