<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;

use JWTAuth;

use Tymon\JWTAuth\Exceptions\JWTException;

use App\Campaign;

class CampaignController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.auth');
    }

    function index() {
        //flag, message, data, errors, status_code
        $status_code = '400';
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $user_id = $user->user_id;

            $cams = Campaign::where('user_id', $user_id)->get();

            if (empty($cams)) {
                $flag = false;
                $message = 'Khong co ke hoach tiet kiem nao';
                $errors[] = 'Khong co ke hoach tiet kiem nao';
                $status_code = '404';
            } else {
                $message = 'Da tim thay';
                $data['campaigns'] = $cams;
                $status_code = '200';
            }
        } else {
            $message = 'Loi';
            $errors[] = $result;
            $status_code = '406';
        }
        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }

    function show($id) {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        $cam_id = $id;

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $user_id = $user->user_id;

            $cam = Campaign::where('user_id', $user_id)->where('cam_id', $cam_id)->first();

            if (empty($cam)) {
                $flag = false;
                $status_code = '404';
                $errors[] = 'Khong tim duoc ke hoach tiet kiem nay';
            } else {
                $message = 'Da tim thay ke hoach tiet kiem';
                $data['campaign'] = $cam;
                $status_code = '200';
            }
        } else {
            $message = "Loi";
            $errors[] = $result;
            $status_code = '406';
        }
        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }

    function store(Request $request) {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $user_id = $user->user_id;
            $cam_data = $request->only('cam_name', 'cam_goal', 'cam_start', 'cam_enddate', 'user_id');
            $validator = Validator::make($cam_data, [
                'cam_name' => 'required|min:3|max:200',
                'cam_start' => 'required|integer|greater_than:0',
                'cam_goal' => 'required|integer|greater_than_field:cam_start',
                'cam_enddate' => 'required|date',
                'user_id' => 'required|exists:users'
            ]);

            if ($validator->fails()) {
                $flag = false;
                $message = 'Thong tin khong hop le';
                $errors[] = $validator->errors();
                $status_code = '406';
            } elseif ($cam_data['user_id'] != $user_id) {
                $flag = false;
                $message = 'Khong duoc phep truy cap';
                $errors[] = 'Khong co quyen tao ke hoach tiet kiem cho tai khoan nay';
                $status_code = '403';
            } else {
                $cam = Campaign::create([
                    'cam_name' => $cam_data['cam_name'],
                    'cam_start' => $cam_data['cam_start'],
                    'cam_goal' => $cam_data['cam_goal'],
                    'cam_enddate' => $cam_data['cam_enddate'],
                    'user_id' => $user_id
                ]);
                $message = 'Da tao ke hoach tiet kiem';
                $data['campaign'] = $cam;
                $status_code = 200;
            }
        } else {
            $message = "Loi";
            $errors[] = $result;
            $status_code = '406';
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }

    function update(Request $request, $id) {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        $cam_id = $id;

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if (!empty($user)) {
            $user_id = $user->user_id;
            $cam_data = $request->only('cam_name', 'cam_goal', 'cam_start', 'cam_enddate', 'user_id');
            $validator = Validator::make($cam_data, [
                'cam_name' => 'min:3|max:200',
                'cam_start' => 'integer|greater_than:0',
                'cam_goal' => 'integer|greater_than_field:cam_start',
                'cam_enddate' => 'date',
                'user_id' => 'required|exists:users'
            ]);

            $cam = Campaign::with(
                array('user' => function($query) {
                    $query->select('user_id');
                }))->find($cam_id);

            if ($validator->fails()) {
                $flag = false;
                $message = 'Thong tin khong hop le';
                $errors[] = $validator->errors();
                $status_code = '406';
            } elseif (empty($cam)) {
                $flag = false;
                $message = 'Khong ton tai ke hoach tiet kiem nay';
                $errors[] = 'Khong ton tai ke hoach tiet kiem nay';
                $status_code = '404';
            } elseif ($cam['user']['user_id'] != $user_id || $cam['user']['user_id'] != $cam_data['user_id']) {
                $flag = false;
                $message = 'Khong co quyen truy cap';
                $errors[] = 'Khong co quyen thay doi ke hoach tiet kiem nay';
                $status_code = '403';
            } else {
                $cam->cam_name = $cam_data['cam_name'] ? $cam_data['cam_name'] : $cam->cam_name;
                $cam->cam_start = $cam_data['cam_start'] ? $cam_data['cam_start'] : $cam->cam_start;
                $cam->cam_goal = $cam_data['cam_goal'] ? $cam_data['cam_goal'] : $cam->cam_goal;
                $cam->cam_enddate = $cam_data['cam_enddate'] ? $cam_data['cam_enddate'] : $cam->cam_enddate;
                $cam->save();
                $message = 'Da cap nhat ke hoach tiet kiem';
                $data['campaign'] = $cam;
                $status_code = 200;
            }
        } else {
            $message = "Loi";
            $errors[] = $result;
            $status_code = '406';
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }

    function destroy($id) {
        $flag = true;
        $message = '';
        $data = array();
        $errors = array();
        $status_code = '400';

        $cam_id = $id;

        $result = $this->getUser($flag);

        $user = $flag ? $result : NULL;

        if ($flag) {
            $user_id = $user->user_id;

            $validator = Validator::make(["cam_id" => $cam_id], [
                'cam_id' => 'exists:campaigns'
            ]);

            $cam = NULL;
            if ($validator->fails()) {
                $flag = false;
                $message = 'Thong tin khong hop le';
                $errors[] = $validator->errors();
                $status_code = '406';
            } else {
                $cam = Campaign::with(
                    array('user' => function($query) {
                        $query->select('user_id');
                    }))->find($cam_id);
            }

            if (!empty($cam) && $cam['user']['user_id'] == $user_id) {
                $cam->delete();

                $message = 'Da xoa ke hoach tiet kiem';
                $status_code = 200;
            } elseif (empty($cam)) {
                $flag = false;
                $message = 'Khong ton tai ke hoach tiet kiem nay';
                $errors[] = 'Khong ton tai ke hoach tiet kiem nay';
                $status_code = '404';
            } elseif ($cam['user']['user_id'] != $user_id) {
                $flag = false;
                $message = 'Khong co quyen truy cap';
                $errors[] = 'Khong co quyen xoa ke hoach tiet kiem nay';
                $status_code = '403';
            }
        } else {
            $message = "Loi";
            $errors[] = $result;
            $status_code = '406';
        }

        return response()->json($this->returnResponse($flag, $message, $data, $errors), $status_code);
    }
}
