<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', [
  'uses' => 'TestJWTController@index'
]);

Route::group(['middleware' => 'cors', 'prefix' => 'api/v1'], function ()
{


    ///////////////////////// CATEGORY /////////////////////////////////////////

 
    Route::get('category/index', 'CategoryController@index');
    Route::get('category/show/{category}', 'CategoryController@show');
    Route::post('category/insert', 'CategoryController@store');
    Route::post('category/update/{category}', 'CategoryController@update');
    Route::post('category/delete/{category}', 'CategoryController@destroy');

    ///////////////////////////////////////////////////////////////////////////


    /////////////////////////// WALLET ////////////////////////////////////////

    Route::get('wallet/index', 'WalletController@index');
    Route::get('wallet/show/{wallet}', 'WalletController@show');
    Route::post('wallet/insert', 'WalletController@store');
    Route::post('wallet/update/{wallet}', 'WalletController@update');
    Route::post('wallet/delete/{wallet}', 'WalletController@destroy');

    //////////////////////////////////////////////////////////////////////////


    ////////////////////////////TRANSACTION///////////////////////////////////

    // Route::resource('transaction', 'TransactionController');
    Route::get('transaction/index', 'TransactionController@index');
    Route::get('transaction/show/{transaction}', 'TransactionController@show');
    Route::post('transaction/insert', 'TransactionController@store');
    Route::post('transaction/update/{transaction}', 'TransactionController@update');
    Route::post('transaction/delete/{transaction}', 'TransactionController@destroy');
    Route::post('transaction/search', 'TransactionController@search');
    Route::get('transaction/getSum', 'TransactionController@getSum');

    ////////////////////////////TRANSACTION//////////////////////////////////


    ////////////////////////////PEOPLE///////////////////////////////////
    //Hien thi cac Transactions co lien quan cua mot People, yeu cau xac thuc
    Route::get('people/{people_id}/transactions', [
        'as' => 'api.v1.people.transactions',
        'uses' => 'PeopleController@listTransactions'
    ]);
    ////////////////////////////////////////////////////////////////////


    ////////////////////////////CAMPAIGN///////////////////////////////////
    //Tao cac route can thiet cho campaign
    Route::resource('campaign', 'CampaignController', [
        'except' => ['create', 'edit']
    ]);
    //////////////////////////////////////////////////////////////////////


    ////////////////////////////USER///////////////////////////////////
    //Dang ky tai khoan moi
    Route::post('user/signup', [
        'as' => 'api.v1.user.signup',
        'uses' => 'UserController@signup'

    ]);
    //Dang nhap bang tai khoan da dang ky
    Route::post('user/login', [
        'as' => 'api.v1.user.login',
        'uses' => 'UserController@login'
    ]);
    //Cho phep nguoi dung sau khi dang nhap co the cap nhat thong tin ca nhan
    Route::patch('user/update', [
        'as' => 'api.v1.user.update',
        'uses' => 'UserController@update'
    ]);
    //Cho phep nguoi dung sau khi dang nhap co the thay doi mat khau tai khoan
    Route::patch('user/changepword', [
        'as' => 'api.v1.user.changepword',
        'uses' => 'UserController@changepword'
    ]);
    //Hien thi thong tin ca nhan cua nguoi dung khi ho da dang nhap
    Route::get('user/info', [
        'as' => 'api.v1.user.info',
        'uses' => 'UserController@info'
    ]);
    ////////////////////////////////////////////////////////////////////
});
