<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{
    protected $table = 'wallets';
    protected $primaryKey = 'wallet_id';
    protected $fillable = [
		'wallet_id',
		'user_id',
		'wallet_name',
		'wallet_description'
	];
    public function transactions(){
    	return $this->hasMany('App\Transaction', 'wallet_id');
    }
    


}
