<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'categories';
    protected $primaryKey = 'cat_id';

    protected $fillable = [
		'cat_id',
		'user_id',
		'cat_name',
		'cat_type',
		'meta_data'
	];

    public function user()
    {
        return $this->belongsTo('App/User');
    }
    public function transactions(){
    	return $this->hasMany('App\Transaction','trans_id');
    }
}
