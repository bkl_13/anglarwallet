<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
    protected $table = 'campaigns';
    protected $primaryKey = 'cam_id';

    protected $fillable = [
        'cam_name', 'cam_start', 'cam_goal', 'cam_enddate', 'user_id'
    ];

    //Mot Campaign phai thuoc ve mot User
    public function user() {
        return $this->belongsTo('App\User');
    }
    
}
